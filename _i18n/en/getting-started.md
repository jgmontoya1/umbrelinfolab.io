# Getting started with Umbrel

If you are a new user of Umbrel, here you have some links and a simple guide about how to use this node.

### **Important links:**

Umbrel Telegram group: [https://t.me/getumbrel](https://t.me/getumbrel)

Umbrel Discord server: [https://discord.gg/VY3SsPZZya](https://discord.gg/VY3SsPZZya)

Umbrel Github page: [https://github.com/getumbrel/umbrel](https://github.com/getumbrel/umbrel)

[Video tutorial (by BTC Sessions)](https://youtu.be/fppmhqjqh2E) about install/setup an Umbrel node from zero

[Hardware tutorial](https://thebitcoinmachines.com/) using RaspPi for Umbrel node

**As a general note**: if you are new into this area of LN (Lightning Network), please educate yourself, at least the basics of LN, BEFORE starting the Umbrel node. Meanwhile your new node is syncing, at least go and read these amazing guide about LN, plenty of information there to give a general aspects about how to use a LN node:

[Lightning Node Management](https://openoms.gitbook.io/lightning-node-management/) – by openoms

[Lightning Network Resources](https://www.lopp.net/lightning-information.html) – by Jameson Lopp

[Wiki Lightning Network](https://wiki.ion.radar.tech/) – by ION Radar Tech

[LightningWiki](https://lightningwiki.net/) – by BitcoinStickers

[Beginners LN Guide](https://bitcoiner.guide/lightning/) – by Bitcoiner Guide

[What is Lightning Network](https://academy.binance.com/en/articles/what-is-lightning-network) – by Binance Academy

[Video tutorials about LN](https://www.youtube.com/user/renepickhardt/videos) – by René Pickhardt

[Video tutorials about LN](https://www.youtube.com/c/MinistryofNodes/videos) – by Ministry of nodes

[Balance of Satoshi](https://github.com/alexbosworth/balanceofsatoshis) - by Alex Bosworth

[An Overview of Lightning Network Implementations](https://medium.com/@fulgur.ventures/an-overview-of-lightning-network-implementations-d670255a6cfa)

### **GENERAL RULES:**

### **1\. Channel size.**

When you open channels, DO NOT start with small amount like 20-50-100k sats. That ridiculous low amount will not be enough not even for the open/close fees. Low amount channels are doing more harm than good, to you and for the rest of the network.

Example: If you have a 20k channel open with my node:

1.  that will barely the open/close fees and remains only dust to spend.
2.  If I want to use that channel to send 50k sats it will not be possible. So the channel will be useless and will lower the node score for both nodes.

Open channels with min 500k-1M sats. That will offer a better routing, for you and for all others that will route txs through your node.

“Bigger is better” **DO NOT** apply too much in this case, so now don’t go to the other extreme and open massive 0.5BTC channels. Much better the approach of having 5-6 outbound channels with each between 500k to 1M sats and also, depending of your needs, 3-4-5 inbound channels, with the same amount.

Now, yes, **more channels is better**, because your node will be better connected and will find a better route and faster.

**2\. Node Operator.**

**Operating a node, doesn’t mean automatically “_I will get rich earning sats_“.** Move away from this wrong mentality.

A. You run the Umbrel node to protect your privacy, to protect your keys, to protect your money, to protect the custody of your wallets.

B. Yes, you will earn some sats, but only if you will manage your node in the right way. But even then, those earnings will be insignificant to be considered “earnings”.

C. You run this node to LEARN more about Bitcoin and find solutions for real life applications. Umbrel is offering that opportunity with many apps included (see LNbits, BTCPay, LN Pool, Whirlpool) ready for use cases.

### **BASIC USAGE:**

**Now, what I suggest, for any new node user, in special with Umbrel:**

1\. Wait to sync the blockchain. It will take time, I know, but be patient. It will get through, no worry. Don’t do anything (stupid) until is not ready. Depending on your hardware configuration and internet speed, it could take between 3 and 12 days. After the sync, it will take another 8-12h to sync the index for Electrum Server.

2\. Keep your software updated (and not just Umbrel, if you are not using the Pi version). So once is synced, check if there are new updates and apply them. If you are using RaspPi option, updating Umbrel will update also the OS. If you are using a Linux machine with Umbrel in docker, you will need to do also: _sudo apt update && apt upgrade._

3\. Install minimum required apps like (see the Umbrel Apps section):

*   Ride The Lightning (RTL) – management for node, wallet, channels, routing.
*   ThunderHub – management for node, wallet, channels, routing, chat, tools.
*   BTC Explorer – Blockchain explorer and tools
*   Mempool – watch mempool fees, blocks, check txs, blockchain explorer, very handy

4\. Optional apps:

*   LNBits – excelent suite of tools – LNURL, LN POS, LNDHUB, Support tickets, Event tickets and many more
*   BTCPay Server – very good backend for webshops/shops, donations, POS, business tool
*   Samourai Whirlpool – privacy and mixing tool, connection for your Samourai wallet
*   Sphinx Chat – very interesting tool for podcasters, private groups chat using LN for tips and chats

5\. Connect some wallets to your node:

The "Connect Wallet" page has easy-to-understand instructions for connecting many wallets. 

6\. **Connect your onchain node wallet with** [**Bluewallet**](https://bluewallet.io/)**.** Yes, now you can connect directly to a mobile app, the onchain AEZEED wallet. Just use your Umbrel node seed in Bluewallet (adding it as new wallet) and done.

Use cases:

*   you want to have at hand a way to deposit quick to your node wallet (onchain)
*   you need to have access to your onchain funds in case your node is crashed and want to recover / access the funds.

### **FUNDING YOUR NODE**

1.  Go to your Umbrel main interface, Bitcoin wallet and click “receive”. From any other wallet send some funds (usually for 3-4 channels, each 1M sats). Wait to be confirmed then go to next step.
2.  You can use Lightning wallet, from the same main Umbrel interface, but I suggest to use Thunderhub app. Why? Because you can control better the miner fees for opening the channel. Also RTL app is good, but still you can’t see how much fee in total you are paying. So use one of these 3 interfaces and go to open channels. Select a node desired and put at least 500k-1M sats and open a channel. Start with one good node (see [https://ln.bigsun.xyz](https://ln.bigsun.xyz/) or [https://1ml.com](https://1ml.com/) )which node has longest history or number of channels) and later continue with other users nodes.
Help new users that post their new nodes URI in the Umbrel Community Telegram group and ask for new channels. TOGETHER STRONGER.

    On [Umbrel's Discord server](https://discord.gg/VY3SsPZZya) you can also find a room where new Umbrel users can exchange info to open mutual channels. That will help a lot the new users, to start with some known node operators and with some liquidity.
    Video Tutorials about how to open/fund LN channels:

*   [Video tutorial Umbrel - by BTC Sessions](https://youtu.be/fppmhqjqh2E)
*   [Video Tutorial RTL app](https://youtu.be/DfRYJcBsfkA?t=155) - by Ministry of Nodes
*   [Video Tutorial ThunderHub app](https://www.youtube.com/watch?v=hZ3ttYFliL4) - by ThunderHub
*   [LN channels mangement - by Alex Bosworth](https://youtu.be/HlPIB6jt6ww)

If you appreciate this mini-guide, you can open a channel with my Umbrel node if you like and I will lower the base fee to 0.9sats for that channel. I usually open a mutual channel if I have available funds.

035e3aa81503b67d61589270c043bcf6dcf083e40bc8630eeeeb08f85570be0fc9@gttoo2zb6vd3h3b2wlauahx3u3ayrzyi6pe2k4eike6rwzcacni6agad.onion:9735

5. Wait for the opening channel to be confirmed (usually 3 confirmations) and done, you are ready to start lightning, using the wallets I indicated before.

#### FEATURES NEEDED IN UMBREL

* I would like to see future Umbrel releases that can open channels using all onchain wallet balance, deducting automatically the fee. Guessing the correct fee always end with some dust sats remaining in the onchain wallet. That's not good.
* Streaming services. Would be nice to have a streaming service, as a separate app. Maybe as pro/paid feature, so the devs can receive something?

#### IN CLOSING

I hope that this mini-guide will help you as a noob to start using your node. Slowly educate yourself and find your own best way to use your node.

There are many other things to do with a node, also many other apps and usages, but this is just the beginning.


### **HAPPY LIGHTNING!**

---

This guide was created by DarthCoin, you can generate a LNUrl [here](https://lntxbot.bigsun.xyz/@DarthCoin). The LNUrl can be read by Zeus Wallet or Thunderhub.
