# Erste Schritte mit Umbrel

Das folgende Tutorial soll neuen Nutzern von Umbrel den Einstieg in Bitcoin & Lightning erleichtern.

### Wichtige Links:

Umbrels Telegramgruppe: [https://t.me/getumbrel](https://t.me/getumbrel)

Umbrels Discordserver: [https://discord.gg/VY3SsPZZya](https://discord.gg/VY3SsPZZya)

Umbrels Github-Seite: [https://github.com/getumbrel/umbrel](https://github.com/getumbrel/umbrel)

[Video-Tutorial (von BTC Sessions)](https://youtu.be/fppmhqjqh2E) zum Installieren / Einrichten eines Umbrel-Nodes

[Hardware-Tutorial](https://thebitcoinmachines.com/) mit RaspPi für Umbrel-Knoten

**Allgemeiner Hinweis**: Wenn du dich mit Lightning noch nicht auskennst, informiere dich bitte zumindest über die Grundlagen des Netzwerks, bevor du anfängst. In der Zwischenzeit kann Umbrel synchronisieren. Die folgenden ausgezeichneten Tutorials, die sehr viele Informationen über Lightning enthalten, sind auf jeden Fall lesenswerzt:

[Lightning Node Management](https://openoms.gitbook.io/lightning-node-management/) - von openoms

[Lightning Network Resources](https://www.lopp.net/lightning-information.html) - von Jameson Lopp

[Wiki Lightning Network](https://wiki.ion.radar.tech/) - von ION Radar Tech

[LightningWiki](https://lightningwiki.net/) - von BitcoinStickers

[LN-Leitfaden für Anfänger](https://bitcoiner.guide/lightning/) - von Bitcoiner Guide

[What is Lightning Network](https://academy.binance.com/de/articles/what-is-lightning-network) - von Binance Academy

[Video-Tutorials zu LN](https://www.youtube.com/user/renepickhardt/videos) - von René Pickhardt

[Video-Tutorials zu LN](https://www.youtube.com/c/MinistryofNodes/videos) - von Ministry of Nodes

[Balance of Satoshi](https://github.com/alexbosworth/balanceofsatoshis) - von Alex Bosworth

[An Overview of Lightning Network Implementations](https://medium.com/@fulgur.ventures/an-overview-of-lightning-network-implementations-d670255a6cfa)

### **ALLGEMEINE REGELN:**

### ** 1\. Kanalgröße. **

Wenn du Kanäle öffnest, beginne NICHT mit einer kleinen Menge wie 20-50-100k Sats. Dieser lächerlich niedrige Betrag wird nicht einmal für die Öffnungs- / Schließungsgebühren ausreichen. Kanäle mit geringer Anzahl schaden dir und dem Rest des Netzwerks mehr als sie nützen.

Beispiel: Wenn due einen 20k-Kanal mit meinem Node geöffnet hast:

1. Dann bleibt nach Öffnungs- und Schließgebühren kaum etwas zum Ausgeben.
2. Wenn ich diesen Kanal zum Senden von 50.000 Sats verwenden möchte, ist dies nicht möglich. Der Kanal ist also unbrauchbar und senkt den Node-Score für beide Knoten.

Öffne Kanäle mit mindestens 500k-1M Sats. Dies bietet ein besseres Routing für dich und für alle, die Sendungen durch dein Node leiten.

"Größer ist besser" solltest du aber **NICHT** zu wörtlich nehmen. Gehe jetzt nicht zum anderen Extrem und öffne  massive 0,5-BTC-Kanäle. Viel besser ist der Ansatz, 5-6 ausgehende Kanäle mit jeweils zwischen 500.000 und 1 Million Sats und je nach Bedarf auch 3-5 eingehende Kanäle mit der gleichen Menge zu haben.

**Mehr Kanäle sind besser**, weil dein Node besser verbunden ist und bessere und schnellere Routen finden kann.

** 2\. Node-Betreibenr **

** Das Betreiben eines Knotens bedeutet nicht automatisch "_Ich werde ein Haufen sats verdienen _". ** Dies stimmt nicht.

A. Du verwendest Umbrel, um deine Privatsphäre, private Keys, Geld und Wallets zu schützen.

B. Ja, du verdienst einige Sats, aber nur, wenn du dein Node richtig verwaltest. Aber selbst dann werden diese Einnahmen zu unbedeutend sein, um als „Einnahmen“ betrachtet zu werden.

C. Du betreibst Umbrel, um mehr über Bitcoin zu erfahren und Lösungen für reale Anwendungen zu finden. Umbrel bietet diese Möglichkeit mit vielen Apps (siehe LNbits, BTCPay, LN Pool, Whirlpool), die für Anwendungsfälle bereit sind.

### ** GRUNDLEGENDE NUTZUNG: **

** Nun, was ich jedem neuen Knotenbenutzer vorschlage, speziell mit Umbrel:**

1. Warte, bis die Blockchain synchronisiert ist. Ich weiß, es wird einige Zeit dauern, aber sei geduldig. Es wird durchkommen, keine Sorge. Mach nichts, bis es nicht fertig ist. Abhängig von deiner Hardwarekonfiguration und der Internetgeschwindigkeit kann dies zwischen 3 und 12 Tagen dauern. Nach der Synchronisierung dauert es weitere 8-12 Stunden, um den Electrumserver, der von einigen Apps genutzt wird, zu starten.

2. Halte die Software auf dem neuesten Stand (und nicht nur Umbrel, wenn du nicht Umbrel OS verwendest). Überprüfe nach der Synchronisierung, ob neue Updates vorhanden sind, und wenden Sie diese an. Auf dem Raspberry Pi mit Umbrel OS wird durch das Aktualisieren von Umbrel auch das Betriebssystem aktualisiert. Auf einem Linux-Computer mit Umbrel in Docker verwenden, kann ein Update folgendermaßen ausgeführt werden: `sudo apt update && apt upgrade`.

3. Installiere diese nützlichen Apps wie (siehe Abschnitt Umbrel-Apps):

* Ride The Lightning (RTL) - Verwaltung für Node, Wallet, Kanäle, Routing.
* ThunderHub - Verwaltung für Node, Wallet, Kanäle, Routing, Chat, Tools.
* BTC Explorer - Blockchain Explorer und Tools
* Mempool - Mempool-Gebühren, Blöcke, Check-Sendungen, Blockchain-Explorer, sehr praktisch

4. Optionale Apps:

* LNBits - hervorragende Tool-Suite - LNURL, LN POS, LNDHUB, Support-Tickets, Event-Tickets und vieles mehr
* BTCPay Server - sehr gutes Backend für Webshops / Shops, Spenden, POS, Business Tool
* Samourai Whirlpool - Datenschutz- und Mischwerkzeug, Anschluss für Ihre Samourai-Brieftasche
* Sphinx Chat - sehr interessantes Tool für Podcaster, private Gruppen chatten mit LN für Tipps und Chats

5. Verbinde Wallets mit deinem Node

Die "Connect Wallet"-Seite hat einfach verständliche Anleitungen zum Verbinden vieler Wallets.

6. ** Verbinden dein Onchain-Wallet mit [Bluewallet](https://bluewallet.io/)**. Mittlerweile kannst du eine direkte Verbindung von deinem Onchain-AEZEED-Wallet zu einer mobilen App herstellen. Dazu einfach ein AEZEED-Wallet aus den 24 Worten in BlueWallet importieren.

Anwendungsfälle:

* Du möchtest eine Möglichkeit zur Hand haben, schnell auf dein Node-Wallet (Onchain) einzuzahlen.
* Du musst Zugriff auf dein Onchain-Wallet haben, falls das Node abstürzt und du dein Guthaben wiederherstellen oder darauf zugreifen möchtest.

### Coins in das Lightning-Wallet senden

1. Öffne das Umbrel-Dashboard, dann das Bitcoin-Wallet und klicke auf "Receive". Sende von anderen Wallets etwas Geld (normalerweise für 3-4 Kanäle, jeweils 1 Million Sats). Warte, bis die Transaktion bestätigt ist.
2. Lightning kannst du zwar direkt über Umbrel verwalten. Ich empfehle jedoch, die Thunderhub-App zu verwenden. Warum? Weil du dort die Mining-Fees für das Öffnen des Kanals besser kontrollieren kannst. Auch die RTL-App ist gut, aber man kann immer noch nicht sehen, wie viel Gebühr insgesamt gezahlt werden muss. Verwende eine dieser 3 Schnittstellen und öffne Kanäle. Wähle einen gewünschten Knoten aus, setze mindestens 500k-1M-Sats ein und öffne einen Kanal. Beginnenmit einem huten Node (siehe [https://ln.bigsun.xyz](https://ln.bigsun.xyz/) oder [https://1ml.com](https://1ml.com/)) welches Node den längsten Verlauf oder die meisten Kanäle hat) und fahre später mit den Nodes anderer Benutzer fort.
Hilf neuen Nutzern, indem du, wenn diese auf Telegram nach Kanälen fragen, auch welche mit ihnen öffnest.

    Auf [Umbrels Discordserver](https://discord.gg/VY3SsPZZya) gibt es auch einen Chatkanal zur Channeleröffnung. Auch so kannst du einigen neuen Nutzern helfen, mit Lightning loszulegen.
    Videotutorials zu LND:

*   [Video tutorial Umbrel - by BTC Sessions](https://youtu.be/fppmhqjqh2E)
*   [Video Tutorial RTL app](https://youtu.be/DfRYJcBsfkA?t=155) - by Ministry of Nodes
*   [Video Tutorial ThunderHub app](https://www.youtube.com/watch?v=hZ3ttYFliL4) - by ThunderHub
*   [LN channels mangement - by Alex Bosworth](https://youtu.be/HlPIB6jt6ww)

If you appreciate this mini-guide, you can open a channel with my Umbrel node if you like and I will lower the base fee to 0.9sats for that channel. I usually open a mutual channel if I have available funds.

035e3aa81503b67d61589270c043bcf6dcf083e40bc8630eeeeb08f85570be0fc9@gttoo2zb6vd3h3b2wlauahx3u3ayrzyi6pe2k4eike6rwzcacni6agad.onion:9735

5. Wait for the opening channel to be confirmed (usually 3 confirmations) and done, you are ready to start lightning, using the wallets I indicated before.

#### FEATURES NEEDED IN UMBREL

* I would like to see future Umbrel releases that can open channels using all onchain wallet balance, deducting automatically the fee. Guessing the correct fee always end with some dust sats remaining in the onchain wallet. That's not good.
* Streaming services. Would be nice to have a streaming service, as a separate app. Maybe as pro/paid feature, so the devs can receive something?

#### IN CLOSING

I hope that this mini-guide will help you as a noob to start using your node. Slowly educate yourself and find your own best way to use your node.

There are many other things to do with a node, also many other apps and usages, but this is just the beginning.




---

Dieses Guide wurde von DarthCoin geschrieben, [hier](https://lntxbot.bigsun.xyz/@DarthCoin) kannst du eine Spenden-LNURL generieren. Die LNURL can von Zeus oder Thunderhub gelesen werden.
