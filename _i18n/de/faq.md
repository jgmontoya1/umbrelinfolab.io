# Häufig gestellte Fragen
{: .no_toc }

Generelle Fragen und weitere Informationen zu der in Umbrel eingesetzten Technologie.

Fragen zu Umbrel und der Verwendung von Umbrel werden auf der [Troubleshooting-Seite](troubleshooting.md) beantwortet.

## Inhaltsverzeichnis
{: .no_toc .text-delta }

1. TOC
{:toc}

### Kann ich durch Lightning-Channel reich werden?

Das weiß niemand. Wahrscheinlich nicht. Du wirst geringste Gebühren bekommen. Ist egal. Viel Spaß bei Umbrel!

### Kann ich die externe SSD mit meinem Windows-Computer lesen?

Ext4 kann auf Windows nur mit externer Software wie [Linux File Systems](https://www.paragon-software.com/home/linuxfs-windows/#faq) von Paragon Software (10-Tage gratis Testen) verwendet werden.

### Kann ich eine bereits heruntergeladene Kopie der Blockchain verwenden, um Zeit zu sparen?

Aus verschiedenen Gründen ist Unterstützung dafür nicht bei Umbrel eingebaut, aber mit dem folgendem Tutorial sollte es trotzdem gehen:

1. Flashe Umbrel OS auf eine SD-Karte und boote den Pi mit dieser SD und der SSD.
2. Führe das normale Setup aus.
3. Öffne die Einstellungen und fahre das Node herunter
4. Entferne die SSD und verbinde sie mit einer Linux-Maschine mit Zugriff auf die heruntergeladene Blockchain.
5. Kopiere die Ordner "blocks" und "chainstate" aus dem Bitcoin Core-Ordner zu /umbrel/bitcoin/ auf der SSD.
6. Entferne die SSD und stecke sie wieder in den Pi, boote diesen dann.

Dies geht nur mit Linux, da es alle Dateisysteme unterstützt (Linux-Dateisysteme wie bei der SSD, aber auch Window- und Mac-Systeme), während die meisten anderen Betriebssysteme bei den Dateisystemen sehr limitiert sind.

### Was machen die ganzen Linux-Befehle?

Dies ist eine (sehr) kurze Liste von häufigen Linux-Befehlen. Für einen Befehl liefert `man [Befehl]` (ohne die eckigen Klammern) mehr Informationen (`q` zum Beenden eingeben).

| Befehl       | Beschreibung                               | Beispiel                                     |
| ------------ | ------------------------------------------ | -------------------------------------------- |
| `cd`         | In einen Ordner wechseln                   | `cd /home/umbrel`                            |
| `ls`         | Ordnerinhalt auflisten                     | `ls -la /home/umbrel/umbrel`                 |
| `cp`         | Kopieren                                   | `cp file.txt newfile.txt`                    |
| `mv`         | Verschieben                                | `mv file.txt moved_file.txt`                 |
| `rm`         | Löschen                                    | `rm temporaryfile.txt`                       |
| `mkdir`      | Ordner erstellen                           | `mkdir /home/umbrel/newdirectory`            |
| `ln`         | Verknüpfung erstellen                      | `ln -s /target_directory /link`              |
| `sudo`       | Befehl as Administrator ausführen          | `sudo nano textfile.txt`                     |
| `su`         | Account wechseln                           | `sudo su root`                               |
| `chown`      | Dateieigentümer ändern                     | `chown umbrel:umbrel myfile.txt`             |
| `chmod`      | Dateiberechtigungen ändern                 | `chmod +x executable.script`                 |
| `nano`       | Texteditor                                 | `nano textfile.txt`                          |
| `tar`        | Archivtool                                 | `tar -cvf archive.tar file1.txt file2.txt`   |
| `exit`       | Aktuelle Sitzung beenden                   | `exit`                                       |
| `systemctl`  | Systemd-Services verwalten                 | `sudo systemctl start umbrel-startup`        |
| `journalctl` | Systemlogs anzeigen                        | `sudo journalctl -u umbrel-external-storage` |
| `htop`       | Prozesse & Ressourcennutzung beobachten    | `htop`                                       |
| `shutdown`   | Den Pi Herunterfahren oder neu starten     | `sudo shutdown -r now`                       |

### Wo gibts mehr Informationen?

Die folgenden Artikel liefern mehr Informationen zu Bitcoin & Lightning:

- [What is Bitcoin?](https://bitcoinmagazine.com/guides/what-bitcoin)
- [Understanding the Lightning Network](https://bitcoinmagazine.com/articles/understanding-the-lightning-network-part-building-a-bidirectional-payment-channel-1464710791/)
- [Bitcoin resources](https://www.lopp.net/bitcoin-information.html) und [Lightning Network resources](https://www.lopp.net/lightning-information.html) von Jameson Lopp

Auf Deutsch gibt's auch Videos vom [Blocktrainer](https://blocktrainer.de).

### Unterstützt Umbrel …?

Aktuell nicht, aber externe Entwickler können es vielleicht im [App Store](https://medium.com/getumbrel/introducing-the-umbrel-app-store-7a2068c64a10) veröffentlichen.

---

Diese Seite  oft mit neuen Entdeckungen aktualisiert. Mach doch mit einem Merge Request mit!

---
