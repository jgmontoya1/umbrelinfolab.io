Guia Nodo Umbrel
================

Si eres un nuevo usuario de este software Umbrel, aquí tienes algunos enlaces y una guía sencilla sobre cómo usar este nodo.

### **Links importantes:**

Umbrel Telegram group: [https://t.me/getumbrel](https://t.me/getumbrel)

Umbrel Discord server: [https://discord.gg/VY3SsPZZya](https://discord.gg/VY3SsPZZya)

Umbrel Github page: [https://github.com/getumbrel/umbrel](https://github.com/getumbrel/umbrel)

[Video tutorial (by BTC Sessions)](https://youtu.be/fppmhqjqh2E) sobre instalar / configurar un nodo Umbrel desde cero

[Hardware tutorial](https://thebitcoinmachines.com/) usando Raspi para el nodo Umbrel

**Como nota general:** Si eres nuevo en esta área de LN (Lightning Network), infórmese, al menos los conceptos básicos de LN, ANTES de iniciar el nodo Umbrel. Mientras tanto, su nuevo nodo se está sincronizando, al menos vaya y lea estas increíble guías sobre LN, hay mucha información allí para brindar aspectos generales sobre cómo usar un nodo LN:

[Lightning Node Management](https://openoms.gitbook.io/lightning-node-management/) – by openoms

[Lightning Network Resources](https://www.lopp.net/lightning-information.html) – by Jameson Lopp

[Wiki Lightning Network](https://wiki.ion.radar.tech/) – by ION Radar Tech

[LightningWiki](https://lightningwiki.net/) – by BitcoinStickers

[Beginners LN Guide](https://bitcoiner.guide/lightning/) – by Bitcoiner Guide

[What is Lightning Network](https://academy.binance.com/en/articles/what-is-lightning-network) – by Binance Academy

[Video tutorials sobre LN](https://www.youtube.com/user/renepickhardt/videos) – by René Pickhardt

[Video tutorials sobre LN](https://www.youtube.com/c/MinistryofNodes/videos) – by Ministry of nodes

[Balance of Satoshi](https://github.com/alexbosworth/balanceofsatoshis) - by Alex Bosworth

[An Overview of Lightning Network Implementations](https://medium.com/@fulgur.ventures/an-overview-of-lightning-network-implementations-d670255a6cfa)

### **REGLAS GENERALES:**

### **1\. Tamaño del canal.**

Cuando abra canales, NO comience con cantidades pequeñas como 20-50-100k sats. Esa cantidad ridículamente baja no será suficiente ni siquiera para las tarifas de apertura / cierre. Los canales de poca cantidad le están haciendo más daño que bien a usted y al resto de la red.

Ejemplo: si tiene un canal de 20k abierto con mi nodo:

1.  que apenas cubre las tarifas de apertura / cierre y solo queda polvo para gastar.
2.  Si quiero usar ese canal para enviar satélites de 50k, no será posible. Por lo tanto, el canal será inútil y reducirá la puntuación del nodo para ambos nodos.

Canales abiertos con min 500k-1M sats. Eso ofrecerá un mejor enrutamiento, para usted y para todos los demás que enrutarán txs a través de su nodo.

“Mas grande es mejor” **NO** se apliqua demasiado en este caso, así que ahora no vaya al otro extremo y abra canales masivos de 0.5BTC. Mucho mejor el enfoque de tener 5-6 canales salientes con cada uno entre 500k a 1M sats y también, dependiendo de sus necesidades, 3-4-5 canales entrantes, con la misma cantidad.

Ahora sí, **más canales es mejor,** porque tu nodo estará mejor conectado y encontrará una ruta mejor y más rápida.

**2\. Operador de nodo.**

**Operar un nodo, no significa automáticamente “_Me haré rico ganando sats_“.** Aléjate de esta mentalidad equivocada.

A. Ejecuta el nodo Umbrel para proteger su privacidad, para proteger sus llaves, para proteger su dinero, para proteger la custodia de sus billeteras.

B. Sí, ganarás algunos sats, pero solo si administras tu nodo de la manera correcta. Pero incluso entonces, esas ganancias serán insignificantes para ser consideradas “ganancias”.

C. Ejecuta este nodo para APRENDER más sobre Bitcoin y encontrar soluciones para aplicaciones de la vida real. Umbrel ofrece esa oportunidad con muchas aplicaciones incluidas (consulte LNbits, BTCPay, LN Pool, Whirlpool) listas para casos de uso.

### **USO BÁSICO:**

**Ahora, lo que sugiero, para cualquier nuevo usuario de nodo, en especial con Umbrel:**

1\. Espere a sincronizar la cadena de bloques. Tomará tiempo, lo sé, pero tenga paciencia. Pasará, no se preocupe. No hagas nada (estúpido) hasta que no esté listo. Dependiendo de la configuración de su hardware y la velocidad de Internet, podría demorar entre 3 y 12 días. Después de la sincronización, tomará otras 8-12 horas sincronizar el índice para Electrum Server.

2\. Mantenga su software actualizado (y no solo Umbrel, si no está usando la versión Pi). Entonces, una vez sincronizado, verifique si hay nuevas actualizaciones y aplíquelas. Si está utilizando la opción RaspPi, la actualización de Umbrel también actualizará el sistema operativo. Si está utilizando una máquina Linux con Umbrel en la ventana acoplable, deberá hacer también: _sudo apt update && apt upgrade_

3\. Instale las aplicaciones mínimas requeridas como (consulte la sección Aplicaciones de Umbrel):

*   Ride The Lightning (RTL) – gestión de nodo, billetera, canales, enrutamiento.
*   ThunderHub – gestión de nodo, billetera, canales, enrutamiento, chat, herramientas.
*   BTC Explorer – Blockchain explorer y utilidades
*   Mempool – ver tarifas de mempool, bloques, verificar txs, explorador de blockchain, muy útil

4\. Aplicacciones opcionales:

*   LNBits – excelente conjunto de herramientas: LNURL, LN POS, LNDHUB, tickets de soporte, tickets de eventos y muchos más
*   BTCPay Server – muy buen backend para tiendas web / tiendas, donaciones, POS, herramienta comercial
*   Samourai Whirlpool – herramienta de privacidad y mezcla, conexión para su billetera Samourai
*   Sphinx Chat – herramienta muy interesante para podcasters, chat de grupos privados usando LN para consejos y chats


5\. Connect some wallets to your node:

The "Connect Wallet" page has easy-to-understand instructions for connecting many wallets. 

6\. **Conecte su billetera de nodo onchain con** [**Bluewallet**](https://bluewallet.io/)**.** Sí, ahora puede conectarse directamente a una aplicación móvil, la billetera AEZEED en cadena. Simplemente use su semilla de nodo Umbrel en Bluewallet (agregándola como nueva billetera) y listo.

Casos de uso:

*   desea tener a mano una forma de depositar rápidamente en su billetera de nodo (onchain)
*   necesita tener acceso a sus fondos en la cadena en caso de que su nodo se bloquee y desee recuperar / acceder a los fondos.

### **FINANCIAR SU NODO**

1.  Vaya a la interfaz principal de Umbrel, billetera Bitcoin y haga clic en “recibir”. Desde cualquier otra billetera envíe algunos fondos (generalmente para 3-4 canales, cada 1 M de satélites). Espere a ser confirmado y luego vaya al siguiente paso.
2.  Puede usar la billetera Lightning, desde la misma interfaz principal de Umbrel, pero sugiero usar la aplicación Thunderhub. ¿Por qué? Porque puedes controlar mejor las tarifas de los mineros por abrir el canal. También la aplicación RTL es buena, pero aún no puede ver la tarifa en total que está pagando. Así que use una de estas 3 interfaces y vaya a canales abiertos. Seleccione un nodo deseado y coloque al menos 500k-1M sats y abra un canal. Comience con un buen nodo (consulte [https://ln.bigsun.xyz](https://ln.bigsun.xyz/) or [https://1ml.com](https://1ml.com/) )qué nodo tiene el historial más largo o el número de canales) y luego continuar con los nodos de otros usuarios.
Help new users that post their new nodes URI in the Umbrel Community Telegram group and ask for new channels. TOGETHER STRONGER.

    On [Umbrel's Discord server](https://discord.gg/VY3SsPZZya) you can also find a room where new Umbrel users can exchange info to open mutual channels. That will help a lot the new users, to start with some known node operators and with some liquidity.
    Video Tutorials about how to open/fund LN channels:

*   [Video tutorial Umbrel - by BTC Sessions](https://youtu.be/fppmhqjqh2E)
*   [Video Tutorial RTL app](https://youtu.be/DfRYJcBsfkA?t=155) - by Ministry of Nodes
*   [Video Tutorial ThunderHub app](https://www.youtube.com/watch?v=hZ3ttYFliL4) - by ThunderHub
*   [LN channels mangement - by Alex Bosworth](https://youtu.be/HlPIB6jt6ww)

If you appreciate this mini-guide, you can open a channel with my Umbrel node if you like and I will lower the base fee to 0.9sats for that channel. I usually open a mutual channel if I have available funds.

035e3aa81503b67d61589270c043bcf6dcf083e40bc8630eeeeb08f85570be0fc9@gttoo2zb6vd3h3b2wlauahx3u3ayrzyi6pe2k4eike6rwzcacni6agad.onion:9735

5. Wait for the opening channel to be confirmed (usually 3 confirmations) and done, you are ready to start lightning, using the wallets I indicated before.

#### FEATURES NEEDED IN UMBREL

* I would like to see future Umbrel releases that can open channels using all onchain wallet balance, deducting automatically the fee. Guessing the correct fee always end with some dust sats remaining in the onchain wallet. That's not good.
* Streaming services. Would be nice to have a streaming service, as a separate app. Maybe as pro/paid feature, so the devs can receive something?

#### IN CLOSING

I hope that this mini-guide will help you as a noob to start using your node. Slowly educate yourself and find your own best way to use your node.

There are many other things to do with a node, also many other apps and usages, but this is just the beginning.


### **HAPPY LIGHTNING!**

---

This guide was created by DarthCoin, you can generate a LNUrl [here](https://lntxbot.bigsun.xyz/@DarthCoin). The LNUrl can be read by Zeus Wallet or Thunderhub.
