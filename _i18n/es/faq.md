# Preguntas Frecuentes
{: .no_toc }

Preguntas generales e información acerca de la teconología usada en Umbrel.

Problemas respecto a las funciones principales de tu nodo de Umbrel se encuentran en otra guía [Solución de Problemas](troubleshooting.md).


## Tabla de contenido
{: .no_toc .text-delta }

1. TOC
{:toc}

### ¿Puedo hacerme rico enrutando pagos de Lightning?

En realidad nadie sabe. Probablemente no. Obtendrás comisiones muy bajas. No me importa. ¡Disfruta la experiencia!

### ¿Puedo insertar el disco duro de formato Ext4 a mi ordenador Windows?

El sistema de archivos Ext4 no es compatible con el Windows estándar, pero con software adicional como [Archivos de Sistema de Linux](https://www.paragon-software.com/home/linuxfs-windows/#faq) por Paragon Software (ofrecen una prueba de 10 días gratis) es posible. 

### ¿Puedo usar una cadena de bloques previamente descargada para ahorrar tiempo en la sincronización de bloques del nodo?

Esto es soportado nativamente por Umbrel por diversas razones, pero puedes hacerlo bajo tu propio riesgo siguiendo las siguientes instrucciones:

1. Flashea el Sistema Operativo Umbrel (Umbrel OS) a una tarjeta Micro SD, insérta tanto la tarjeta Micro SD como el SSD, y enciende el nodo
2. Configura tu nodo (por el Dashboard/menú principal)
3. Ve al apartado de configuración (Settings)(En el Dashboard/menú principal) y apaga tu nodo Umbrel (Tu SSD ya fue configurado y formateado para poder ser usado con Umbrel)
4. Descontecta el SSD y conéctalo a una máquina anfitrión Linux con accesso a la cadena de bloques previamente descargada
5. Copia los directorios “blocks” y “chainstate” de tu directorio de datos Bitcoin Core y pégalos en el directorio /umbrel/bitcoin/ del SSD que usarás con tu nodo.
6. Desconecta el SSD del anfitrión, conéctalo de nuevo a tu nodo, y enciéndelo.

### ¿Cuál es la función de los comandos de Linux?

Esta es una (muy) pequeña lista de comandos de Linux comunes para tu referencia. Para saber la función específica de un solo comando,
escribe `man [nombre del comando]` para desplegar un manual acerca de este (presiona `q` para salir).

| comando      | descripción                            | ejemplo                                      |
| ------------ | ---------------------------------------| -------------------------------------------- |
| `cd`         | cambiar directorio                     | `cd /home/umbrel`                            |
| `ls`         | contenido del directorio               | `ls -la /home/umbrel/umbrel`                 |
| `cp`         | copiar                                 | `cp archivo.txt newfile.txt`                 |
| `mv`         | mover                                  | `mv archivo.txt archivomovido_file.txt`      |
| `rm`         | remover                                | `rm archivotemporal.txt`                     |
| `mkdir`      | crear directorio                       | `mkdir /home/umbrel/nuevodirectorio`         |
| `ln`         | crear un enlace                        | `ln -s /directorio_objetivo /link`           |
| `sudo`       | ejecutar comandos como superusuario    | `sudo nano textfile.txt`                     |
| `su`         | cambiar de cuenta                      | `sudo su root`                               |
| `chown`      | cambiar el dueño delarchivo            | `chown umbrel:umbrel myfile.txt`             |
| `chmod`      | cambiar permisos del archivo           | `chmod +x executable.script`                 |
| `nano`       | editor de archivos de texto            | `nano textfile.txt`                          |
| `tar`        | herramienta para archivos              | `tar -cvf archive.tar file1.txt file2.txt`   |
| `exit`       | salir de la sesión actual              | `exit`                                       |
| `systemctl`  | controlar el servicio systemd          | `sudo systemctl start umbrel-startup`        |
| `journalctl` | consultar el proceso de systemd        | `sudo journalctl -u umbrel-external-storage` |
| `htop`       | monitorear procesos y gasto de energía |  `htop`                                      |
| `shutdown`   | apagar o resetear Raspberry Pi         | `sudo shutdown -r now`                       |

### ¿Dónde puedo obtener más información?

Si tienes curiosidad acerca de cómo funciona Bitcoin y la Lightning Network y quieres aprender más,
los siguientes artículos y revistas te ayudarán a comprender estos temas más profundamente:

En español:
- La revista [Dinero Sin Reglas](https://dinerosinreglas.com/) por Decentralized_b
- [Nodos de enrutado en la Lightning Network](https://estudiobitcoin.com/nodos-de-enrutado-en-la-lightning-network/) por csh2000

En inglés:
- [What is Bitcoin?](https://bitcoinmagazine.com/guides/what-bitcoin) y [Understanding the Lightning Network](https://bitcoinmagazine.com/articles/understanding-the-lightning-network-part-building-a-bidirectional-payment-channel-1464710791/) por Bitcoin Magazine
- [Bitcoin resources](https://www.lopp.net/bitcoin-information.html) y [Lightning Network resources](https://www.lopp.net/lightning-information.html) por Jameson Lopp

### ¿Umbrel soporta…?

No actualmente, pero Umbrel tiene una infraestructura de aplicaciones con el fin de que desarrolladores terceros puedan añadir aplicaciones a Umbrel para publicarlas en la [Tienda de Aplicaciones](https://medium.com/getumbrel/introducing-the-umbrel-app-store-7a2068c64a10).

---

Esta guía de Preguntas Frecuentes será actualizada con fallos que han o serán reportados en la sección de errores. Se aprecian aportaciones y recomendaciones.

---
