## Recursos adicionales de Umbrel
{: .no_toc }

Esta página presenta información adicional acerca de Umbrel que no puede ser encontrada en su página oficial.
La intención de esta página es responder a preguntas generales acerca de Umbrel.

---

 Esta no es una página oficial de Umbrel.
 Esta guía sólo aplica para la última versión de Umbrel (actualmente v0.3.7).

---

## Tabla de contenido
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Acerca de esta guía

### Estructura

1. Introducción (esta página)
2. [Getting started](getting-started.html): Getting started with Umbrel.
3. [Solución de Problemas](troubleshooting.html): Problemas acerca las funciones principales de tu nodo Umbrel.
4. [Preguntas Frecuentes](faq.html): Preguntas generales e información acerca de la tecnología usada en Umbrel.

---

## Una pequeña advertencia

Umbrel es un proyecto aún en fase Beta. Con lo cual no recomendamos tener más dinero en tu nodo Umbrel del que estés dispuesto a perder.

---

Esta página es parte de [Umbrel Labs](https://umbrel.tech), un servicio gratuito para expandir tu nodo Umbrel.

---
